import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StringForExam {

    static String returnDublicateLetters(String str){
        Map<String, Long> map = Stream.of(str).flatMap(s -> Arrays.stream(s.split("")))
                .collect(Collectors.toMap(Function.identity(), v -> 1L, Long::sum));

        return map.keySet().stream()
                .map(key -> key.toUpperCase() + map.get(key))
                .collect(Collectors.joining("", "", ""));
    }

    public static void main(String[] args) {
        System.out.println(returnDublicateLetters("aaabbcc"));


    }


}
